import Vue from 'vue';
import axios from 'axios';

// initial state
const state = {
  records: [],

  formRecord: {},
  editMode: false,
  showForm: false,


  page: 1,
  totalPages: 0,
}

// mutations
const mutations = {

  SET_RECORDS(state, records) {
    state.records = records;
  },

  SET_TOTAL_PAGES(state, totalPages) {
    state.totalPages = totalPages;
  },

  SET_PAGE(state, page) {
    state.page = page;
  },
  
  SET_FILTERS(state, filters) {
    state.filters = filters;
  },

  SET_FORM_RECORD(state, formRecord) {
    state.formRecord = formRecord;
  },

  SET_EDIT_MODE(state, mode) {
    state.editMode = mode;
  },

  SHOW_FORM(state) {
    state.showForm = true;
  },

  HIDE_FORM(state) {
    state.showForm = false;
  },

  APPEND_RECORD(state, record) {
    state.records.push(JSON.parse(JSON.stringify(record)));
  },

  UPDATE_RECORD(state, record) {
    let recordIndex = state.records.findIndex((item) => {
      return item.id == record.id;
    });
    Vue.set(state.records, recordIndex, JSON.parse(JSON.stringify(record)));
  },

  DELETE_RECORD(state, recordId) {
    state.records.forEach((item, index) => {
      if (item.id == recordId) state.records.splice(index, 1);
    });
  },
}

// actions
const actions = {

  fetchRecords({ state, commit }, params) {
    // for (const property in state.filters) {
    //   if (state.filters[property] == '') {
    //     delete state.filters[property];
    //   }
    // }

    // console.log(params.page);
    // if (!params.page) {
    //   params.page = state.page;
    // }
    // else {
      commit('SET_PAGE', parseInt(params.page));
    // }


    // console.log(state.filters);

    return axios.get('/admin/products', { params: { ...params, ...state.filters } })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },


  
  fetchAllRecords({ state, commit }) {
    return axios.get('/admin/products/all')
      .then((response) => {
        commit('SET_RECORDS', response.data);
        // commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },

  // =========================================================================

  searchRecords({ commit }, query) {
    return axios.get('/products/search?q=' + query)
      .then((response) => {
        commit('SET_RECORDS_SEARCH', response.data);
      });
  },

  // =========================================================================

  create({ commit }, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (!form.hasOwnProperty(property) || form[property] == null) {
        continue;
      }

      if (Array.isArray(form[property]) && form[property].length > 0) {
        for (const iterator of form[property]) {
          payload.append(property + '[]', iterator);
        }
      }
      else {
        payload.append(property, form[property]);
      }
    }

    return axios.post('/admin/products', payload)
      .then((response) => {
        commit('APPEND_RECORD', response.data.record);
      });
  },

  // =========================================================================

  update({}, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (!form.hasOwnProperty(property) || form[property] == null) {
        continue;
      }

      if (Array.isArray(form[property])) {
        for (const iterator of form[property]) {
          payload.append(property + '[]', iterator);
        }
      }
      else {
        payload.append(property, form[property]);
      }
    }

    return axios.post(`/admin/products/${form.id}`, payload);
  },

  // =========================================================================

  delete({ commit }, { recordId }) {
    return axios.delete(`/admin/products/${recordId}`)
      .then((response) => {
        commit('DELETE_RECORD', recordId);
        return response;
      });
  }
}

export default {
  namespaced: true,
  name: 'products',
  state,
  actions,
  mutations
}
