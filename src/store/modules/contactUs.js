import axios from 'axios';

// initial state
const state = {
  // record info
  currentRecord: {},
  showRecordInfo: false,

  // paginate records
  filters: { page: 1 },
  totalPages: 0,
  records: [],
}

// mutations
const mutations = {

  // record info
  SET_CURRNET_RECORD(state, record) {
    state.currentRecord = record;
  },

  SHOW_RECORD_INFO(state) {
    state.showRecordInfo = true;
  },

  HIDE_RECORD_INFO(state) {
    state.showRecordInfo = false;
  },

  // paginate records
  SET_FILTERS(state, filters) {
    for (const key in filters) {
      if (filters[key] == '') {
        filters[key] = undefined;
      }
    }
    state.filters = filters;
  },

  SET_TOTAL_PAGES(state, totalPages) {
    state.totalPages = totalPages;
  },

  SET_RECORDS(state, records) {
    state.records = records;
  },

  DELETE_RECORD(state, recordId) {
    state.records.forEach((item, index) => {
      if (item.id == recordId) state.records.splice(index, 1);
    });
  },
}

// actions
const actions = {

  fetchRecords({ state, commit }) {
    return axios.get('/admin/contact-us', { params: state.filters })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },

  // =========================================================================

  delete({ commit }, { recordId }) {
    return axios.delete(`/admin/contact-us/${recordId}`)
      .then((response) => {
        commit('DELETE_RECORD', recordId);
        return response;
      });
  },

  // =========================================================================

  markAsSeen({}, { recordId }) {
    return axios.post(`/admin/contact-us/${recordId}/mark-as-seen`);
  }
}

export default {
  namespaced: true,
  name: 'contactUs',
  state,
  actions,
  mutations
}
