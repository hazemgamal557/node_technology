import Vue from 'vue';
import axios from 'axios';

// initial state
const state = {
  // record form
  recordForm: {},
  editMode: false,
  showRecordForm: false,

  // paginate records
  filters: { page: 1 },
  totalPages: 0,
  records: [],
}

// mutations
const mutations = {

  // paginate records
  SET_TOTAL_PAGES(state, totalPages) {
    state.totalPages = totalPages;
  },

  SET_FILTERS(state, filters) {
    for (const key in filters) {
      if (filters[key] == '') {
        filters[key] = undefined;
      }
    }
    state.filters = filters;
  },

  SET_RECORDS(state, records) {

    state.records = records;

  },

  // record form
  SET_RECORD_FORM(state, recordForm) {
    state.recordForm = recordForm;
  },

  SET_EDIT_MODE(state, mode) {
    state.editMode = mode;
  },

  SHOW_RECORD_FORM(state) {
    state.showRecordForm = true;
  },

  HIDE_RECORD_FORM(state) {
    state.showRecordForm = false;
  },

  // manage
  UPDATE_RECORD(state, record) {
    let index = state.records.findIndex(item => item.id === record.id) ;
    state.records.splice(index, 1, record);
  },

  DELETE_RECORD(state, recordId) {
    state.records.forEach((item, index) => {
      if (item.id == recordId) state.records.splice(index, 1);
    });
  },

 

}

// actions
const actions = {

  fetchData({ state, commit }, { query }) {
    return axios.get('/admin/shops/search?q=' + query , { params: state.filters })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);

      });
  },

  fetchRecords({ state, commit }) {
    return axios.get('/admin/shops', { params: state.filters })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },

  fetchRecords2({ state, commit }) {
    return axios.get('/admin/shops/unverified', { params: state.filters })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },


  // =========================================================================

  create({}, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (!form.hasOwnProperty(property) || form[property] === null) {
        continue;
      }

      if (Array.isArray(form[property])) {
        for (const iterator of form[property]) {
          payload.append(property + '[]', iterator);
        }
      }
      else {
        payload.append(property, form[property]);
      }
    }

    return axios.post('/admin/shops/add', payload);
  },

  // =========================================================================

  update({}, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (!form.hasOwnProperty(property) || form[property] === null) {
        continue;
      }

      if (Array.isArray(form[property])) {
        for (const iterator of form[property]) {
          payload.append(property + '[]', iterator);
        }
      }
      else {
        payload.append(property, form[property]);
      }
    }

    return axios.post(`/admin/shops/${form.id}/verify`, payload);
  },

  // =========================================================================


  
  verify({ commit }, { record }) {

    return axios.post(`/admin/shops/${record.id}/verify`)
    
    },
 
  delete({ commit }, { recordId }) {
    return axios.delete(`/admin/shops/${recordId}/delete`)
      .then((response) => {
        commit('DELETE_RECORD', recordId);
        // const currentIndex = this.phones.findIndex(p => p.id === id);
        // this.phones.splice(currentIndex, 1, updatedPhone)

        return response;
      });
  },

  
 
}

export default {
  namespaced: true,
  name: 'shops',
  state,
  actions,
  mutations
}
