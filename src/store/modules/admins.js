import Vue from 'vue';
import axios from 'axios';

// initial state
const state = {
  records: [],

  formRecord: {},
  editMode: false,
  showForm: false,

  page: 1,
  totalPages: 0,
}

// mutations
const mutations = {

  SET_RECORDS(state, records) {
    state.records = records;
  },

  SET_TOTAL_PAGES(state, totalPages) {
    state.totalPages = totalPages;
  },

  SET_PAGE(state, page) {
    state.page = page;
  },

  SET_FORM_RECORD(state, formRecord) {
    state.formRecord = formRecord;
  },

  SET_EDIT_MODE(state, mode) {
    state.editMode = mode;
  },

  SHOW_FORM(state) {
    state.showForm = true;
  },

  HIDE_FORM(state) {
    state.showForm = false;
  },

  APPEND_RECORD(state, record) {
    state.records.push(JSON.parse(JSON.stringify(record)));
  },

  UPDATE_RECORD(state, record) {
    let recordIndex = state.records.findIndex((item) => {
      return item.id == record.id;
    });
    Vue.set(state.records, recordIndex, JSON.parse(JSON.stringify(record)));
  },

  DELETE_RECORD(state, recordId) {
    state.records.forEach((item, index) => {
      if (item.id == recordId) state.records.splice(index, 1);
    });
  },
}

// actions
const actions = {

  fetchRecords({ state, commit }, { page, limit }) {
    if (!page) {
      page = state.page;
    }
    else {
      commit('SET_PAGE', page);
    }
    if (!limit) limit = 15;

    return axios.get('/admin/', { params: { page, limit } })
      .then((response) => {
        commit('SET_RECORDS', response.data.data);
        commit('SET_TOTAL_PAGES', response.data.last_page);
      });
  },

  // =========================================================================

  searchRecords({ commit }, query) {
    return axios.get('/search?q=' + query)
      .then((response) => {
        commit('SET_RECORDS_SEARCH', response.data);
      });
  },

  // =========================================================================

  create({ commit }, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (form.hasOwnProperty(property) && form[property] != null)
        payload.append(property, form[property]);
    }

    return axios.post('/admin/', payload)
      .then((response) => {
        commit('APPEND_RECORD', response.data.record);
      });
  },

  // =========================================================================

  update({ dispatch }, { form }) {
    let payload = new FormData;
    for (const property in form) {
      if (form.hasOwnProperty(property) && form[property] != null)
        payload.append(property, form[property]);
    }

    return axios.post(`/admin/update-my-profile`, payload)
      .then(() => {
        dispatch('fetchRecords', {});
      });
  },

  // =========================================================================

  delete({ commit }, { recordId }) {
    return axios.delete(`/admin/${recordId}`)
      .then((response) => {
        commit('DELETE_RECORD', recordId);
        return response;
      });
  }
}

export default {
  namespaced: true,
  name: 'admins',
  state,
  actions,
  mutations
}
