import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  rtl: true,
  theme: {
    themes: {
      light: {
        primary: '#19456a',
        secondary: '#19456a',
        accent: '#19456a',
        success: '#51a057',
        error: '#af4545'
      }
    }
  }
});
//#fb7925
//#4184ea