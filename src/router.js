import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/home',
      meta: { auth: true },
      component: () => import('./App.vue'),
      children: [
        {
          path: 'home',
          name: 'home',
          component: () => import('./views/home/Home.vue')
        },
        {
          path: 'categories',
          name: 'categories',
          component: () => import('./views/categories/Categories.vue')
        },
     
        {
          path: 'users',
          name: 'users',
          component: () => import('./views/users/Users.vue')
        },
        {
          path: 'orders',
          name: 'orders',
          component: () => import('./views/orders/Orders.vue')
        },
        {
          path: 'orders/:id/:user',
          name: 'orderDetails',
          component: () => import('./views/orders/Single.vue')
        },

        {
          path: 'admins',
          name: 'admins',
          component: () => import('./views/admins/Admins.vue')
        },
        {
          path: 'products',
          name: 'products',
          component: () => import('./views/products/Products.vue')
        },
        {
          path: 'sliders',
          name: 'sliders',
          component: () => import('./views/sliders/Sliders.vue')
        },
        // Products
        {
          path: 'products/:id',
          name: 'single-product',
          component: () => import('./views/products/single/Index.vue')
        },
       
        {
          path: 'notifications',
          name: 'notifications',
          component: () => import('./views/notifications/Notifications.vue')
        },
        {
          path: 'contact-us',
          name: 'contact-us',
          component: () => import('./views/contactUs/ContactUs.vue')
        },
        {
          path: 'social',
          name: 'social',
          component: () => import('./views/social/Social.vue')
        },
        {
          path: 'settings',
          name: 'settings',
          component: () => import('./views/settings/Settings.vue')
        },
        {
          path: 'admin/pages/:page',
          name: 'settings',
          component: () => import('./views/settings/Settings.vue')
        },
       ]
    },

    // other routes
    {
      path: '/login',
      name: 'Login',
      meta: { auth: false },
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/403',
      name: 'unauthorized',
      component: () => import('./views/Unauthorized.vue')
    },
    {
      path: '*',
      name: 'Not Found',
      component: () => import('./views/NotFound.vue')
    }
  ]
});

Vue.router = router;
export default router;