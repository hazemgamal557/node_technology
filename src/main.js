import Vue from 'vue';
import '@mdi/font/css/materialdesignicons.css';

// router
import router from './router.js';

// store
import store from './store';

window._ = require('lodash');
window.Fire= new Vue();
// bus object
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(Chartkick.use(Chart))
Vue.prototype.$bus = new Vue();

// plugins
import './plugins/axios';
import './plugins/vueauth';
import './plugins/mixins';
import vuetify from './plugins/vuetify';

// instantiation
Vue.config.productionTip = false;
import EntryPoint from './EntryPoint.vue';

// global components
Vue.component('back-button', require('./components/BackButton.vue').default);

new Vue({
  store,
  router,
  vuetify,
  render: h => h(EntryPoint)
}).$mount('#app') 
